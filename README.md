# Level durations

[![build status](https://gitlab.interactivesystems.info/fp_social_game_interaction/LevelDurations/badges/master/build.svg)](https://gitlab.interactivesystems.info/fp_social_game_interaction/LevelDurations/commits/master)

## Downloads der Grafik

Diese Downloads repräsentieren den aktuellen Stand des `master`-Branches:

* [Spieldauer pro Level](http://fp-sgi.s3-website.eu-central-1.amazonaws.com/LevelDurations/LevelDurations.pdf) (PDF)
* [Spieldauer pro Level](http://fp-sgi.s3-website.eu-central-1.amazonaws.com/LevelDurations/LevelDurations.jpg) (JPEG)
